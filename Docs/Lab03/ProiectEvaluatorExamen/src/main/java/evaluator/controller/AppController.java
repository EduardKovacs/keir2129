package evaluator.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import evaluator.exception.InputValidationFailedException;
import evaluator.model.Intrebare;
import evaluator.model.Statistica;
import evaluator.model.Test;
import evaluator.repository.IntrebariRepository;
import evaluator.exception.DuplicateIntrebareException;
import evaluator.exception.NotAbleToCreateStatisticsException;
import evaluator.exception.NotAbleToCreateTestException;

public class AppController {
	private IntrebariRepository intrebariRepository;
	private static final String _fileName = "/Users/eduardk/Documents/an3sem2/vvss/keir2129/Docs/Lab02/ProiectEvaluatorExamen/src/main/java/evaluator/data/intrebari.txt";

	public AppController() {
		intrebariRepository = new IntrebariRepository(_fileName);
	}

	public AppController(String givenFilename) {
		intrebariRepository = new IntrebariRepository(givenFilename);
	}
	
	public Intrebare addNewIntrebare(String enunt, String varianta1, String varianta2, String varianta3, String raspunsCorect, String domeniu) throws DuplicateIntrebareException, InputValidationFailedException, IOException {
		Intrebare intrebare = new Intrebare(enunt, varianta1, varianta2, varianta3, raspunsCorect, domeniu);
		intrebariRepository.addIntrebare(intrebare);
		
		return intrebare;
	}

	public List<Intrebare> getIntrebari()
	{
		for (Intrebare intrebare : intrebariRepository.getIntrebari()) {
			System.out.println(intrebare.toString());
		}

		return intrebariRepository.getIntrebari();
	}
	
	public boolean exists(Intrebare intrebare){
		return intrebariRepository.exists(intrebare);
	}
	
	public Test createNewTest() throws NotAbleToCreateTestException{
		if(intrebariRepository.getIntrebari().size() < 5)
			throw new NotAbleToCreateTestException("Nu exista suficiente intrebari pentru crearea unui test!(5)");
		
		if(intrebariRepository.getNumberOfDistinctDomains() < 5)
			throw new NotAbleToCreateTestException("Nu exista suficiente domenii pentru crearea unui test!(5)");
		
		List<Intrebare> testIntrebari = new LinkedList<Intrebare>();
		List<String> domenii = new LinkedList<String>();
		Intrebare intrebare;
		Test test = new Test();
		
		while(testIntrebari.size() != 5){
			intrebare = intrebariRepository.pickRandomIntrebare();

			if(!testIntrebari.contains(intrebare) && !domenii.contains(intrebare.getDomeniu())){
				testIntrebari.add(intrebare);
				domenii.add(intrebare.getDomeniu());
			}
		}
		
		test.setIntrebari(testIntrebari);
		return test;
	}
	
	public void loadIntrebariFromFile(String f){
		intrebariRepository.setIntrebari(intrebariRepository.loadIntrebariFromFile());
	}
	
	public Statistica getStatistica() throws NotAbleToCreateStatisticsException{
		
		if(intrebariRepository.getIntrebari().isEmpty())
			throw new NotAbleToCreateStatisticsException("Repository-ul nu contine nicio intrebare!");
		
		Statistica statistica = new Statistica();
		for(String domeniu : intrebariRepository.getDistinctDomains()){
			statistica.add(domeniu, intrebariRepository.getIntrebari().size());
		}
		
		return statistica;
	}

}
