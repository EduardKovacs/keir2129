package evaluator.main;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import evaluator.exception.DuplicateIntrebareException;
import evaluator.exception.InputValidationFailedException;
import evaluator.exception.NotAbleToCreateTestException;
import evaluator.model.Intrebare;
import evaluator.model.Statistica;

import evaluator.controller.AppController;
import evaluator.exception.NotAbleToCreateStatisticsException;
import evaluator.model.Test;

//functionalitati
//F01.	 adaugarea unei noi intrebari pentru un anumit domeniu (enunt intrebare, raspuns 1, raspuns 2, raspuns 3, raspunsul corect, domeniul) in setul de intrebari disponibile;
//F02.	 crearea unui nou test (testul va contine 5 intrebari alese aleator din cele disponibile, din domenii diferite);
//F03.	 afisarea unei statistici cu numarul de intrebari organizate pe domenii.

public class StartApp {
	public static void main(String[] args) throws IOException, DuplicateIntrebareException, InputValidationFailedException {
		BufferedReader console = new BufferedReader(new InputStreamReader(System.in));
		AppController appController = new AppController();
		
		boolean activ = true;
		String optiune = null;
		
		while(activ){
			
			System.out.println("");
			System.out.println("1.Adauga intrebare");
			System.out.println("2.Creeaza test");
			System.out.println("3.Statistica");
			System.out.println("4.Exit");
			System.out.println("");
			
			optiune = console.readLine();
			
			switch(optiune){
			case "1" :
				String enunt;
				String raspuns1;
				String raspuns2;
				String raspuns3;
				String raspunsCorect;
				String domeniu;

				System.out.println("Enunt intrebare: ");
				enunt = console.readLine();

				System.out.println("Raspuns1: ");
				raspuns1 = console.readLine();

				System.out.println("Raspuns2: ");
				raspuns2 = console.readLine();

				System.out.println("Raspuns3: ");
				raspuns3 = console.readLine();

				System.out.println("Raspuns Corect: ");
				raspunsCorect = console.readLine();

				System.out.println("Domeniu: ");
				domeniu = console.readLine();

				appController.addNewIntrebare(enunt, raspuns1, raspuns2, raspuns3, raspunsCorect, domeniu);
				break;
			case "2" :
				try {
					Test test = appController.createNewTest();
					for (Intrebare intrebare : test.getIntrebari()) {
						System.out.println(intrebare.toString());
					}
				} catch (NotAbleToCreateTestException e) {
					System.out.println(" O crepat");
					e.printStackTrace();
				}
				break;
			case "3" :
				try {
					Statistica statistica = appController.getStatistica();
					System.out.println("Statistica: ");
					for (Intrebare intrebare :
							statistica.getIntrebariDomenii()) {

					}
				} catch (NotAbleToCreateStatisticsException e) {
					e.printStackTrace();
				}
				break;
			case "4" :
				activ = false;
				break;
			default:
				break;
			}
		}
	}
}
