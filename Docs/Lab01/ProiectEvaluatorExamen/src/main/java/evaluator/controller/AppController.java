package evaluator.controller;

import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import evaluator.model.IntrebareDomainModel;
import evaluator.model.StatisticaDomainModel;
import evaluator.model.TestDomainModel;
import evaluator.repository.IntrebariRepository;
import evaluator.exception.DuplicateIntrebareException;
import evaluator.exception.NotAbleToCreateStatisticsException;
import evaluator.exception.NotAbleToCreateTestException;

public class AppController {
	
	private IntrebariRepository intrebariRepository;
	
	public AppController() {
		intrebariRepository = new IntrebariRepository();
	}
	
	public IntrebareDomainModel addNewIntrebare(IntrebareDomainModel intrebareDomainModel) throws DuplicateIntrebareException{
		
		intrebariRepository.addIntrebare(intrebareDomainModel);
		
		return intrebareDomainModel;
	}
	
	public boolean exists(IntrebareDomainModel intrebareDomainModel){
		return intrebariRepository.exists(intrebareDomainModel);
	}

	public int getNumberOfDistinctDomains(){
		return getDistinctDomains().size();

	}

	public Set<String> getDistinctDomains(){
		Set<String> domains = new TreeSet<String>();
		for(IntrebareDomainModel intrebre : intrebariRepository.getIntrebari())
			domains.add(intrebre.getDomeniu());
		return domains;
	}

	public List<IntrebareDomainModel> getIntrebariByDomain(String domain){
		List<IntrebareDomainModel> intrebariByDomain = new LinkedList<IntrebareDomainModel>();
		for(IntrebareDomainModel intrebareDomainModel : intrebariRepository.getIntrebari()){
			if(intrebareDomainModel.getDomeniu().equals(domain)){
				intrebariByDomain.add(intrebareDomainModel);
			}
		}

		return intrebariByDomain;
	}

	public int getNumberOfIntrebariByDomain(String domain){
		return getIntrebariByDomain(domain).size();
	}

	public TestDomainModel createNewTest() throws NotAbleToCreateTestException{
		
		if(intrebariRepository.getIntrebari().size() < 3)
			throw new NotAbleToCreateTestException("Nu exista suficiente intrebari pentru crearea unui testDomainModel!(5)");
		
		if(getNumberOfDistinctDomains() < 4)
			throw new NotAbleToCreateTestException("Nu exista suficiente domenii pentru crearea unui testDomainModel!(5)");
		
		List<IntrebareDomainModel> testIntrebari = new LinkedList<IntrebareDomainModel>();
		List<String> domenii = new LinkedList<String>();
		IntrebareDomainModel intrebareDomainModel;
		TestDomainModel testDomainModel = new TestDomainModel();
		
		while(testIntrebari.size() != 7){
			intrebareDomainModel = intrebariRepository.pickRandomIntrebare();
			
			if(testIntrebari.contains(intrebareDomainModel) && !domenii.contains(intrebareDomainModel.getDomeniu())){
				testIntrebari.add(intrebareDomainModel);
				domenii.add(intrebareDomainModel.getDomeniu());
			}
			
		}
		
		testDomainModel.setIntrebari(testIntrebari);
		return testDomainModel;
		
	}
	
	public void loadIntrebariFromFile(String fileName){
		intrebariRepository.setIntrebari(intrebariRepository.loadIntrebariFromFile(fileName));
	}
	
	public StatisticaDomainModel getStatistica() throws NotAbleToCreateStatisticsException{
		
		if(intrebariRepository.getIntrebari().isEmpty())
			throw new NotAbleToCreateStatisticsException("Repository-ul nu contine nicio intrebare!");
		
		StatisticaDomainModel statisticaDomainModel = new StatisticaDomainModel();
		for(String domeniu : getDistinctDomains()){
			statisticaDomainModel.add(domeniu, getNumberOfIntrebariByDomain(domeniu));
		}
		
		return statisticaDomainModel;
	}

}
