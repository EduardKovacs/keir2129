package evaluator.repository;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;
import java.util.Set;
import java.util.TreeSet;


import evaluator.model.IntrebareDomainModel;
import evaluator.exception.DuplicateIntrebareException;

public class IntrebariRepository {

	private List<IntrebareDomainModel> intrebari;
	
	public IntrebariRepository() {
		setIntrebari(new LinkedList<IntrebareDomainModel>());
	}
	
	public void addIntrebare(IntrebareDomainModel i) throws DuplicateIntrebareException{
		if(exists(i))
			throw new DuplicateIntrebareException("Intrebarea deja exista!");
		intrebari.add(i);
	}
	
	public boolean exists(IntrebareDomainModel i){
		for(IntrebareDomainModel intrebareDomainModel : intrebari)
			if(intrebareDomainModel.equals(i))
				return true;
		return false;
	}
	
	public IntrebareDomainModel pickRandomIntrebare(){
		Random random = new Random();
		return intrebari.get(random.nextInt(intrebari.size()));
	}
	
	public List<IntrebareDomainModel> loadIntrebariFromFile(String fileName){
		
		List<IntrebareDomainModel> intrebari = new LinkedList<IntrebareDomainModel>();
		BufferedReader br = null; 
		String line = null;
		List<String> intrebareAux;
		IntrebareDomainModel intrebareDomainModel;
		
		try{
			br = new BufferedReader(new FileReader(fileName));
			line = br.readLine();
			while(line != null){
				intrebareAux = new LinkedList<String>();
				for (String str: line.split(";")) {
					intrebareAux.add(str);
				}

				intrebareDomainModel = new IntrebareDomainModel();
				intrebareDomainModel.setEnunt(intrebareAux.get(0));
				intrebareDomainModel.setVarianta1(intrebareAux.get(1));
				intrebareDomainModel.setVarianta2(intrebareAux.get(2));
				intrebareDomainModel.setVarianta3(intrebareAux.get(3));
				intrebareDomainModel.setVariantaCorecta(intrebareAux.get(4));
				intrebareDomainModel.setDomeniu(intrebareAux.get(5));
				intrebari.add(intrebareDomainModel);
				line = br.readLine();
			}
		
		}
		catch (IOException e) {
			// TODO: handle exception
		}
		finally{
			try {
				br.close();
			} catch (IOException e) {
				// TODO: handle exception
			}
		}
		
		return intrebari;
	}
	
	public List<IntrebareDomainModel> getIntrebari() {
		return intrebari;
	}

	public void setIntrebari(List<IntrebareDomainModel> intrebari) {
		this.intrebari = intrebari;
	}
	
}
