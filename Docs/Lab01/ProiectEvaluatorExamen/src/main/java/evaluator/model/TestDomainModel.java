package evaluator.model;

import java.util.LinkedList;
import java.util.List;

public class TestDomainModel {

	private List<IntrebareDomainModel> intrebari;
	
	public TestDomainModel() {
		intrebari = new LinkedList<IntrebareDomainModel>();
	}
	
	public List<IntrebareDomainModel> getIntrebari() {
		return intrebari;
	}
	
	public void setIntrebari(List<IntrebareDomainModel> intrebari) {
		this.intrebari = intrebari;
	}
}
