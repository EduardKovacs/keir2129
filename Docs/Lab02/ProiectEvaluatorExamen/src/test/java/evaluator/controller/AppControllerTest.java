package evaluator.controller;

import evaluator.exception.DuplicateIntrebareException;
import evaluator.exception.InputValidationFailedException;
import evaluator.exception.NotAbleToCreateTestException;
import evaluator.model.Intrebare;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;

import static org.junit.Assert.*;

public class AppControllerTest {
    private AppController appController;
    private Intrebare intrebare;

    @Before
    public void setUp() throws Exception {
        appController = new AppController();
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void addNewIntrebare_TC1_EP() {
        int initialCount = appController.getIntrebari().size();
        try {
            appController.addNewIntrebare("Cat face 3+3?", "1) 2", "2) 4", "3) 6", "1", "Matematica");
            assertEquals(appController.getIntrebari().size(), initialCount + 1);
        } catch (InputValidationFailedException |IOException e) {
            assertTrue(false);
        } catch (DuplicateIntrebareException e) {
            assertTrue(true);
        }
    }

    @Test
    public void addNewIntrebare_TC2_EP() {
        try {
            appController.addNewIntrebare("Cat face 3+3?", "1) 2", "2) 4", "3) 6", "4", "Matematica");
            assertTrue(false);
        } catch (InputValidationFailedException e) {
            if(e.getMessage().equals("Varianta corecta nu este unul dintre caracterele {'1', '2', '3'}")) {
                assertTrue(true);
            }
            else {
                assertTrue(false);
            }
        } catch (DuplicateIntrebareException | IOException e) {
            assertTrue(false);
        }
    }

    @Test
    public void addNewIntrebare_TC3_EP() {
        try {
            appController.addNewIntrebare("Cat face 3+3?", "1) 2", "2) 4", "3) 6", "2", "matematica");
            assertTrue(false);
        } catch (InputValidationFailedException e) {
            if(e.getMessage().equals("Prima litera din domeniu nu e majuscula!")) {
                assertTrue(true);
            }
            else {
                assertTrue(false);
            }
        } catch (DuplicateIntrebareException | IOException e) {
            assertTrue(false);
        }
    }

    @Test
    public void addNewIntrebare_TC4_EP() {
        try {
            appController.addNewIntrebare("Cat face 3+3?", "1) 2", "2) 4", "3) 6", "2", "");
            assertTrue(false);
        } catch (InputValidationFailedException e) {
            if(e.getMessage().equals("Domeniul este vid!")) {
                assertTrue(true);
            }
            else {
                assertTrue(false);
            }
        } catch (DuplicateIntrebareException | IOException e) {
            assertTrue(false);
        }
    }

    @Test
    public void addNewIntrebare_TC5_EP() {
        String long31CharactersDomainName = new String("FTRql3g7wmVQc2V2quwrWk8WdtLX13F");
        try {
            appController.addNewIntrebare("Cat face 3+3?", "1) 2", "2) 4", "3) 6", "2", long31CharactersDomainName);
            assertTrue(false);
        } catch (InputValidationFailedException e) {
            if(e.getMessage().equals("Lungimea domeniului depaseste 30 de caractere!")) {
                assertTrue(true);
            }
            else {
                assertTrue(false);
            }
        } catch (DuplicateIntrebareException | IOException e) {
            assertTrue(false);
        }
    }

    @Test
    public void addNewIntrebare_TC1_BVA() {
        try {
            appController.addNewIntrebare("Cat face 3+3?", "1) 2", "2) 4", "3) 6", "0", "Matematica");
            assertTrue(false);
        } catch (InputValidationFailedException e) {
            if(e.getMessage().equals("Varianta corecta nu este unul dintre caracterele {'1', '2', '3'}")) {
                assertTrue(true);
            }
            else {
                assertTrue(false);
            }
        } catch (DuplicateIntrebareException | IOException e) {
            assertTrue(false);
        }
    }

    @Test
    public void addNewIntrebare_TC2_BVA() {
        int initialCount = appController.getIntrebari().size();
        String long29CharactersDomainName = new String("MateUsSinyIOwofhgHvLiUfCptP29");

        try {
            appController.addNewIntrebare("Cat face 3+3?", "1) 2", "2) 4", "3) 6", "1", long29CharactersDomainName);
            assertEquals(appController.getIntrebari().size(), initialCount + 1);
        } catch (InputValidationFailedException | IOException e) {
            assertTrue(false);
        } catch (DuplicateIntrebareException e) {
            assertTrue(true);
        }
    }

    @Test
    public void addNewIntrebare_TC3_BVA() {
        String long31CharactersDomainName = new String("MateUsSinyIOwofhgHvLiUfCptPaa31");
        try {
            appController.addNewIntrebare("Cat face 3+3?", "1) 2", "2) 4", "3) 6", "2", long31CharactersDomainName);
            assertTrue(false);
        } catch (InputValidationFailedException e) {
            if(e.getMessage().equals("Lungimea domeniului depaseste 30 de caractere!")) {
                assertTrue(true);
            }
            else {
                assertTrue(false);
            }
        } catch (DuplicateIntrebareException | IOException e) {
            assertTrue(false);
        }
    }

    @Test
    public void addNewIntrebare_TC4_BVA() {
        int initialCount = appController.getIntrebari().size();

        try {
            appController.addNewIntrebare("Cat face 3+3?", "1) 2", "2) 4", "3) 6", "1", "M");
            assertEquals(appController.getIntrebari().size(), initialCount + 1);
        } catch (InputValidationFailedException | IOException e) {
            assertTrue(false);
        } catch (DuplicateIntrebareException e) {
            assertTrue(true);
        }
    }

    @Test
    public void addNewIntrebare_TC5_BVA() {
        int initialCount = appController.getIntrebari().size();

        try {
            appController.addNewIntrebare("Cat face 3+3?", "1) 2", "2) 4", "3) 6", "1", "");
            assertEquals(appController.getIntrebari().size(), initialCount + 1);
        } catch (InputValidationFailedException e) {
//            if e
        } catch (DuplicateIntrebareException | IOException e) {
            assertTrue(false);
        }
    }

    @Test
    public void createNewTest_F02_TC01() {
        appController = new AppController("/Users/eduardk/Documents/an3sem2/vvss/keir2129/Docs/Lab02/ProiectEvaluatorExamen/src/test/java/evaluator/controller/intrebari_F02_TC01.txt");

        try {
            appController.createNewTest();
            assertTrue(false);
        } catch (NotAbleToCreateTestException e) {
            assertTrue(true);
        }
    }

    @Test
    public void createNewTest_F02_TC02() {
        appController = new AppController("/Users/eduardk/Documents/an3sem2/vvss/keir2129/Docs/Lab02/ProiectEvaluatorExamen/src/test/java/evaluator/controller/intrebari_F02_TC02.txt");

        try {
            appController.createNewTest();
            assertTrue(false);
        } catch (NotAbleToCreateTestException e) {
            assertTrue(true);
        }
    }

    @Test
    public void createNewTest_F02_TC03() {
        appController = new AppController("/Users/eduardk/Documents/an3sem2/vvss/keir2129/Docs/Lab02/ProiectEvaluatorExamen/src/test/java/evaluator/controller/intrebari_F02_TC03.txt");

        try {
            evaluator.model.Test test = appController.createNewTest();
            assertTrue(test.getIntrebari().size() == 5);
        } catch (Exception e) {
            assertTrue(false);
        }
    }
}